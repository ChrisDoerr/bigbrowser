﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows;

namespace BigBrowserUi
{
    public class AppState
    {

        /**
         * Since the Properties.Settings.Default data has been set to USER scope,
         * it won't be saved/updated in the App Direcory but in
         * C:\Users\{User}\AppData\Local\BigBrowserUi\BigBrowserUi.exe_Url_{someHash}\{version}\user.config
         *
         * Not 100% sure but cleaning this up should be considered when UNINSTALLING an application.
         * Even when the app itself does not require INSTALLATION!
         */
        private string _defaultPath;
        private string _currentPath;
        private string _currentFile;
        private string _savePath;

        public string DefaultPath {
            get {
                return _defaultPath;
            }
            set
            {
                // In App State
                _defaultPath = value;

                // In App.config
                Properties.Settings.Default.defaultPath = _defaultPath;

                // Save the data immediately
                Properties.Settings.Default.Save();
            }
        }
        public string CurrentPath {
            get
            {
                return _currentPath;
            }
            set
            {
                // In App State
                _currentPath = value;

                // In App.config
                Properties.Settings.Default.currentPath = _currentPath;

                // Save the data immediately
                Properties.Settings.Default.Save();
            }
        }
        public string CurrentFile {
            get {
                return _currentFile;
            }
            set
            {
                // In App State
                _currentFile = value;

                // In App.config
                Properties.Settings.Default.currentFile = _currentFile;

                // Save the data immediately
                Properties.Settings.Default.Save();

            }
        }
        public string SavePath
        {
            get {
                return _savePath;
            }
            set
            {
                // In App State
                _savePath = value;

                // In App.config
                Properties.Settings.Default.savePath = _savePath;

                // Save the data immediately
                Properties.Settings.Default.Save();

            }
        }

        public string AppPath { get; set; }

        public AppState()
        {

            AppPath     = System.AppDomain.CurrentDomain.BaseDirectory;

            GetAppState();

        }

        public void GetAppState()
        {

            DefaultPath = Properties.Settings.Default.defaultPath;

            CurrentPath = Properties.Settings.Default.currentPath;
            CurrentFile = Properties.Settings.Default.currentFile;

        }

        public void ResetData()
        {

            CurrentPath = Properties.Settings.Default.defaultPath;
            CurrentFile = "";

            Properties.Settings.Default.Save();

            // To also reset the properties of this OBJECT/CLASS
            GetAppState();

        }

    }

}
