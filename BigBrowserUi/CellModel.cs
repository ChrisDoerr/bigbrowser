﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using BigBrowserUi.UserControls;

namespace BigBrowserUi
{
    /**
     * Eine Zelle besteht aus einem Bild, dazugehörigen Properties, (evtl) einem Active/Inactive State
     */
    public class CellModel
    {

        // In a GRID of only columns (to display one image in one column
        public int ColumnIndex { get; set; }

        public string CurrentPath { get; set; }
        public string CurrentFilename { get; set; }

        public string CurrentFile {
            get
            {
                return CurrentPath + CurrentFilename;
            }
        }

        // List of all files (for prev/next browsing)
        public List<string> DirectoryFiles { get; set; }

        public Border borderControl;
        public Image imageControl;

        public UCCell CellControl;

        // Constructor
        public CellModel( int columnIndex, string currentPath )
        {

            ColumnIndex                         = columnIndex;

            CurrentPath                         = currentPath;
            CurrentFilename                     = "";

            CellControl                         = new UCCell();


            /*
            borderControl                       = new Border();
            borderControl.BorderThickness       = new Thickness( 1, 1, 1, 1 );
            borderControl.BorderBrush           = ((SolidColorBrush)(new BrushConverter().ConvertFrom("#A00")));

            imageControl                        = new Image();

            imageControl.Stretch                = Stretch.Uniform;
            imageControl.StretchDirection       = StretchDirection.DownOnly;
            imageControl.VerticalAlignment      = VerticalAlignment.Center;
            imageControl.HorizontalAlignment    = HorizontalAlignment.Center;
            */



        }

    }
}
