﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.IO;

namespace BigBrowserUi
{

    public class ImageSourceProp : INotifyPropertyChanged
    {

        private string _filename;

        public string Filename {
            get
            {
                return _filename;
            }
            set
            {

                if( File.Exists( value ) )
                {

                    _filename = value;

                    OnPropertyChanged("Filename");

                }

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {

            if( PropertyChanged != null )
            {
                PropertyChanged( this, new PropertyChangedEventArgs(property) );
            }

        }

    }

}
