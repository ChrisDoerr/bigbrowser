﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using NaturalSort.Extension; // Absolute life saver!! https://github.com/tompazourek/NaturalSort.Extension


namespace BigBrowserUi
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /* General App */
        public AppState State { get; set; }

        /// <summary>
        /// _windowMod=0 => window; _windowMode=1 => full screen
        /// </summary>
        private int _windowMode;

        private bool shortcutsPopupExists;

        public string RootPath { get; private set; }

        public string PathSeparator { get; private set; }


        /* Grid Manager */
        public int NumberOfColumns { get; set; }

        public int currentCellIndex;

        public List<CellModel> Cells { get; set; }

        public string CurrentCellImageFile {
            get
            {
                return Cells[ currentCellIndex ].CurrentFile;
            }
        }

        public List<string> ImagesInDirectory { get; set; }

        public MainWindow()
        {

            InitializeComponent();

            State               = new AppState();

            Cells               = new List<CellModel>();

            ImagesInDirectory   = new List<string>();

            NumberOfColumns     = 0;

            _windowMode         = 0;

            shortcutsPopupExists = false;

            PathSeparator       = System.IO.Path.DirectorySeparatorChar.ToString();
            RootPath            = AppDomain.CurrentDomain.BaseDirectory + PathSeparator;

            // Initialize first cell
            AddNewCellToGrid( false );

        }

        public void AddNewCellToGrid( bool OpenDirectoryDialogue )
        {

            currentCellIndex = Cells.Count;

            CellModel Cell = new CellModel(currentCellIndex, State.CurrentPath);

            // Add Cell to list/collection.
            Cells.Add(Cell);

            // Increase number of columns accordingly.
            NumberOfColumns += 1;

            // Each and every column should have the very same width.
            ColumnDefinition newColumnDefinition    = new ColumnDefinition() { Width = new GridLength( 1, GridUnitType.Star ) }; // Width=1* or width=*

            MasterGrid.ColumnDefinitions.Add( newColumnDefinition );

            // Add the cell's image control to the column
            //Grid.SetColumn( Cell.imageControl, (NumberOfColumns - 1) );
            Grid.SetColumn( Cell.CellControl, (NumberOfColumns - 1) );


            MasterGrid.Children.Add( Cell.CellControl );

            if( OpenDirectoryDialogue )
            {
                OpenFileDialogue();
            }

            // Alle anderen anpassen?

        }

        public void OpenFileDialogue()
        {

            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();

            openFileDlg.InitialDirectory = Cells[currentCellIndex].CurrentPath;
            openFileDlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

            Nullable<bool> result = openFileDlg.ShowDialog();

            if (result == true)
            {

                string selectedFile     = openFileDlg.FileName;
                string selectedPath     = System.IO.Path.GetDirectoryName( selectedFile ) + System.IO.Path.DirectorySeparatorChar;  // System.IO.Path.DirectorySeparatorChar als ENUM erstellen!?
                string selectedFilename = System.IO.Path.GetFileName( selectedFile );

                // @todo check path and file exists!
                if (Cells[currentCellIndex].CurrentPath != selectedPath)
                {

                    Cells[currentCellIndex].CurrentPath = selectedPath;
                    State.CurrentPath = selectedPath;

                }

                if (Cells[currentCellIndex].CurrentFilename != selectedFilename)
                {

                    Cells[currentCellIndex].CurrentFilename = selectedFilename;

                }

                // Compile a list of all image files in the selected path.
                ImagesInDirectory.Clear();
                ImagesInDirectory = GetImagesInDirectory( selectedPath );

                LoadImageIntoCell();

            }

        }

        public void LoadImageIntoCell()
        {

            /*
            BitmapImage originalBitmap  = new BitmapImage();

            originalBitmap.BeginInit();
            originalBitmap.UriSource    = new Uri( Cells[ currentCellIndex ].CurrentFile );
            originalBitmap.EndInit();
            */
            //            Cells[ currentCellIndex ].imageControl.Source = originalBitmap;
            Cells[currentCellIndex].CellControl.ImageSource.Filename = Cells[ currentCellIndex ].CurrentFile;

            // MessageBox.Show(Cells[currentCellIndex].CellControl.ImageSource);

        }

        public List<string> GetImagesInDirectory( string directory ) {

            string[] allFiles   = Directory.GetFiles( directory );

            List<string> images = new List<string>();

            foreach ( string file in allFiles )
            {

                string fileExtension = System.IO.Path.GetExtension( file ).ToLower();

                if( fileExtension == ".jpg" || fileExtension == ".jpeg" || fileExtension == ".png" )
                {
                    images.Add( file );
                }

            }

            // Absolute life saver!! https://github.com/tompazourek/NaturalSort.Extension
            var ordered = images.OrderBy( x => x, StringComparer.OrdinalIgnoreCase.WithNaturalSort() );

            List<string> result = ordered.ToList();

            return result;

        }

        public void LoadPreviousImageIntoCell()
        {

            // @todo check if file exists!!!!!

            string currentImageFile = Cells[currentCellIndex].CurrentFile;

            int index               = ImagesInDirectory.IndexOf(currentImageFile);

            int previousIndex       = ( index == 0 ? (ImagesInDirectory.Count -1 ) : ( index -1 ) );


            Cells[ currentCellIndex ].CurrentFilename = System.IO.Path.GetFileName( ImagesInDirectory[ previousIndex ] );

            LoadImageIntoCell();

        }

        public void LoadNextImageIntoCell()
        {

            // @todo check if file exists!!!!!

            string currentImageFile = Cells[ currentCellIndex ].CurrentFile;

            int index               = ImagesInDirectory.IndexOf( currentImageFile );

            int nextIndex           = ( index < (ImagesInDirectory.Count -1 ) ? (index + 1) : 0 );


            Cells[ currentCellIndex ].CurrentFilename = System.IO.Path.GetFileName( ImagesInDirectory[ nextIndex ] );

            LoadImageIntoCell();

        }

        public void ToggleWindowMode()
        {

            if( _windowMode == 0 )
            {

                /**
                 * At least on (my) Windows 10 (instance) you have to collapse the visibility
                 * before making the "full screen" changes
                 * and make the window visible when the changes have applied.
                 * Only then will also the task bar covered!!
                 */
                MasterWindow.Visibility     = Visibility.Collapsed;
                MasterWindow.WindowStyle    = WindowStyle.None;
                MasterWindow.ResizeMode     = ResizeMode.NoResize;
                MasterWindow.WindowState    = WindowState.Maximized;
                MasterWindow.Visibility     = Visibility.Visible;

                _windowMode = 1;

            }
            else
            {
                MasterWindow.WindowStyle = WindowStyle.SingleBorderWindow;
                MasterWindow.ResizeMode = ResizeMode.CanResize;
                _windowMode = 0;
            }

        }

        public void ActivateNextCell()
        {

            int nextCellIndex = ( currentCellIndex +1 );

            if(nextCellIndex >= Cells.Count )
            {
                nextCellIndex = 0;
            }

            // @todo indicate currentCell by maybe showing a border for a second or so?
            currentCellIndex = nextCellIndex;

        }

        public void ShowShortcutsInfoPopUp()
        {

            if( shortcutsPopupExists == true )
            {
                return;
            }

            Window popup = new Window()
            {
                Title                   = "Keyboard Shortcuts",
                Width                   = 600,
                Height                  = 350,
                WindowStartupLocation   = WindowStartupLocation.CenterScreen,
                Content                 = new UserControls.UCKeyboardShortcutsInfo()
            };

            shortcutsPopupExists = true;

            popup.Closing += CloseShortcutsInfoPopUp;

            popup.Show();

        }

        private void CloseShortcutsInfoPopUp( object sender, System.ComponentModel.CancelEventArgs e )
        {
            shortcutsPopupExists = false;
        }


        public void SaveScreenshot()
        {

            string mixdownPath = RootPath + "Mixdowns" + PathSeparator;

            if( !Directory.Exists( mixdownPath ) )
            {
                Directory.CreateDirectory(mixdownPath);
            }


            string filename = "Mixdown-" + DateTime.Now.ToString("yyyyMMdd-hhmmss") + ".png";
            string file     = mixdownPath + filename;


            // https://stackoverflow.com/questions/34837286/wpf-take-a-screenshot-and-save-it#answer-34837527
            double screenLeft   = SystemParameters.VirtualScreenLeft;
            double screenTop    = SystemParameters.VirtualScreenTop;
            double screenWidth  = SystemParameters.VirtualScreenWidth;
            double screenHeight = SystemParameters.VirtualScreenHeight;

            using (Bitmap bmp = new Bitmap((int)screenWidth,
                (int)screenHeight))
            {
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    Opacity = .0;
                    g.CopyFromScreen((int)screenLeft, (int)screenTop, 0, 0, bmp.Size);
                    bmp.Save(file);
                    Opacity = 1;
                }

            }

            MessageBox.Show("Mixdown image has been saved.");

        }

        private void MainWindow_KeyDownListener( object sender, KeyEventArgs e )
        {

            /* ======== KEY COMBINATIONS ======== */

            // The COMBINATIONS have to be detected before the single key events!

            // (Left) CTRL + O for Open Browse Directory Dialogue
            if ( Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.O )
            {

                OpenFileDialogue();

                // MessageBox.Show("Open Browse Directory Dialogue");

                return;
            }

            // (Left) CTRL + S for saving the current image as mixdown as well as the "meta state" (basically, two files, the image file and the .xml meta file)
            if ( Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.S )
            {

                // @todo only in FULL SCREEN mode?!
                SaveScreenshot();

                // MessageBox.Show("SAVE current image as mixdown + meta xml");
                return;

            }

            // (Left) CTRL + ARROW RIGHT for moving the current CELL one to the right - if there is more than once cell in the grid. Loopable.
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.Right)
            {
                MessageBox.Show("MOVE CELL to the right");
                return;
            }

            // (Left) CTRL + ARROW LEFT for moving the current CELL one to the left - if there is more than one cell in the grid. Loopable.
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.Left)
            {
                MessageBox.Show("MOVE CELL to the left");
                return;
            }

            // (LEFT) CTRL + "+" for adding a new cell.
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.OemPlus)
            {
                AddNewCellToGrid( true );
                return;
            }


            /* ======== SINGLE KEY EVENTS ======== */

            // SPACEBAR or ARROW RIGHT (or D as in WASD) for browsing to the NEXT image in the current directory. Loopable.
            if ( e.Key == Key.Space || e.Key == Key.Right || e.Key == Key.D )
            {
                LoadNextImageIntoCell();
                return;
            }

            // BACKSPACE or ARROW LEFT (or A as in WASD) for browsing to the PREVIOUS image in the current directory. Loopable.
            if ( e.Key == Key.Back|| e.Key == Key.Left || e.Key == Key.A )
            {
                LoadPreviousImageIntoCell();
                return;
            }

            // + for zooming INTO an image (making it bigger)
            if ( e.Key == Key.OemPlus )
            {
                MessageBox.Show("ZOOM INTO an image");
                return;
            }

            // - for zooming OUT of an image (making it smaller)
            if ( e.Key == Key.OemMinus )
            {
                MessageBox.Show("ZOOMING OUT of an image");
                return;
            }

            // R for rotating an image 90 degrees to the right.
            if ( e.Key == Key.R )
            {
                MessageBox.Show("ROTATE image 90 degrees to the RIGHT");
                return;
            }

            // L for rotating an image 90 degree to the left.
            if ( e.Key == Key.L )
            {
                MessageBox.Show("ROTATE image 90 degrees to the LEFT");
                return;
            }

            // DEL for deleting the current cell - if there is is more than once cell in the grid.
            if ( e.Key == Key.Delete )
            {
                MessageBox.Show("DELETE current CELL");
                return;
            }

            // RETURN for toggling between "full screen" and "window" mode.
            if( e.Key == Key.Return )
            {
                ToggleWindowMode();
            }

            // TAB for "jumping"/focusing/activating the NEXT CELL (making it "the current cell")
            if( e.Key == Key.Tab )
            {
                ActivateNextCell();
            }

            // I [or F12(?)] for opening a popup showing all the keyboard shortcuts and maybe other information as well.
            if( e.Key == Key.I )
            {
                ShowShortcutsInfoPopUp();
                return;
            }

        }

    }

}
