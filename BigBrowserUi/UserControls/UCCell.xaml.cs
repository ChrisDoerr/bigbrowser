﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BigBrowserUi.UserControls
{
    /// <summary>
    /// Interaction logic for UCCell.xaml
    /// </summary>
    public partial class UCCell : UserControl
    {

        public ImageSourceProp ImageSource { get; set; }

        public UCCell()
        {

            // ImageSource = @"C:\_alteMultimediaDaten\Images\Flickr.com\LQ\01\050109-dresden-hundbeisst.jpg";
            // DataContext = this;

            InitializeComponent();

            ImageSource = new ImageSourceProp();

            DataContext = ImageSource;

        }

    }
}
