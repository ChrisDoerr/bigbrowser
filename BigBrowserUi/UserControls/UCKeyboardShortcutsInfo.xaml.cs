﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BigBrowserUi.UserControls
{
    /// <summary>
    /// Interaction logic for UCKeyboardShortcutsInfo.xaml
    /// </summary>
    public partial class UCKeyboardShortcutsInfo : UserControl
    {

        ObservableCollection<ShortcutInfo> Shortcuts = new ObservableCollection<ShortcutInfo>();

        public UCKeyboardShortcutsInfo()
        {

            InitializeComponent();

            /* COMBOS */
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "(Left) CTRL + O", Description = "Load image" });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "(Left) CTRL + S", Description = "Saving the current screen as image (mixdown)." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "(Left) CTRL + ARROW RIGHT", Description = "Moving the current panel one to the right." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "(Left) CTRL + ARROW LEFT", Description = "Moving the current panel one to the left." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "(Left) CTRL + \"+\"", Description = "Add a new panel." });

            /* SINGLE KEYS */
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "SPACEBAR or ARROW RIGHT", Description = "Show the NEXT image in the current directory." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "BACKSPACE or ARROW LEFT", Description = "Show the PREVIOUS image in the current directory." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "+", Description = "Zoom into the current panels's image." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "-", Description = "Zoom out of the current panels's image." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "R", Description = "Rotate the current panel's image 90 degrees to the RIGHT." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "L", Description = "Rotate the current panel's image 90 degrees to the LEFT." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "DEL", Description = "Remove the current panel." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "RETURN", Description = "TOGGLE between \"full screen\" and \"window mode\"." });
            Shortcuts.Add(new ShortcutInfo() { Shortcut = "TAB", Description = "Skip to the NEXT panel (making it the \"current\" panel)." });

            uiShortcutsInfo.ItemsSource = Shortcuts;

        }

    }
}
