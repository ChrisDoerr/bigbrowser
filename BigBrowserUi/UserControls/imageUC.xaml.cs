﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BigBrowserUi.UserControls
{
    /// <summary>
    /// Interaction logic for imageUC.xaml
    /// </summary>
    public partial class imageUC : UserControl
    {

        public string  ImageSource { get; set; }

        public imageUC()
        {

            DataContext = this;

            InitializeComponent();

        }
    }
}
